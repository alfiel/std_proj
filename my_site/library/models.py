from operator import truediv
from django.db import models

# Create your models here.

class Author(models.Model):
    name = models.CharField(
        verbose_name = 'ФИО',
        max_length = 100
    )
    b_date = models.DateField(
        verbose_name = 'Дата рождения',
        null = True,
        blank = True
    )
    current_writer = models.BooleanField(verbose_name = 'Действующий автор',
        default = False
    )
    info = models.TextField(verbose_name = 'Информация')
    def __str__(self):
        return self.name

class Book(models.Model):
    book_name = models.CharField(
        verbose_name = 'Название',
        max_length = 100
    )
    author = models.ForeignKey('Author', on_delete = models.CASCADE)
    write_date = models.DateField(
        verbose_name = 'Дата написания',
        null = True,
        blank = True
    )
    genre = models.ForeignKey('Genre', on_delete = models.CASCADE)
    num_of_pages = models.IntegerField(
        verbose_name = 'Кол-во страниц'
    )
    book_info = models.TextField(verbose_name = 'О книге')
    def __str__(self):
        return self.book_name

class Genre(models.Model):
    genre_name = models.CharField(
        verbose_name = 'Жанр',
        max_length = 100
    )
    types = models.TextField(verbose_name = 'Виды жанра')
    genre_info = models.TextField(verbose_name = 'О жанре')
    def __str__(self):
        return self.genre_name