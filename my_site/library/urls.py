from django.urls import path
from .views import *
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'authors', AuthorViewSet, basename = 'Authors')
router.register(r'books', BookViewSet, basename = 'Books')
router.register(r'genres', GenreViewSet, basename = 'Genres')

urlpatterns = router.urls